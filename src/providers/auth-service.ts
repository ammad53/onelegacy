import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase, FirebaseObjectObservable } from 'angularfire2/database';
import { GlobalVariable } from '../app/globals';
import firebase from 'firebase';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';

@Injectable()
export class AuthService {

  public fireAuth: any;
  public userData: any;
  item: FirebaseObjectObservable<any>;
  membershipSubscription: any;

  constructor(
    private afAuth: AngularFireAuth,
    private globals: GlobalVariable,
    private db: AngularFireDatabase) {

    console.log('Hello AuthService Provider');
    afAuth.authState.subscribe(user => {
      if (user) {
        this.fireAuth = user
        console.log(user);
      }
    });
    // this.fireAuth = firebase.auth();
    // this.afAuth.auth.currentUser.photoURL = "https://s-media-cache-ak0.pinimg.com/736x/85/b4/cf/85b4cf112cf40c191ca5676b726fc410.jpg"
    this.userData = firebase.database().ref('/users');
  }

  loginUser(email: string, password: string): firebase.Promise<any> {
    // return firebase.auth().signInWithEmailAndPassword(email, password);
    // this.afAuth.auth.currentUser.photoURL = "https://s-media-cache-ak0.pinimg.com/736x/85/b4/cf/85b4cf112cf40c191ca5676b726fc410.jpg"
    return this.afAuth.auth.signInWithEmailAndPassword(email, password);
  }

  async signupUser(email: string, password: string, signupData: any)/* : firebase.Promise<any> */ {
    const newUser = await this.afAuth.auth.createUserWithEmailAndPassword(email, password);
    await this.afAuth.auth.currentUser.updateProfile({
      displayName: signupData.userName,
      photoURL: "https://s-media-cache-ak0.pinimg.com/736x/85/b4/cf/85b4cf112cf40c191ca5676b726fc410.jpg"
    });
    this.globals.current_userUID = newUser.uid;
    this.globals.userName = signupData.userName
    await this.userData.child(newUser.uid)
      .set(
      {
        email: email,
        user_name: signupData.userName,
        date_of_birth: signupData.dob,
        contact: signupData.contact,
        address: signupData.address,
        secondaryContact: signupData.secondaryContact,
        membership: "Free"
      }
      );
  }

  resetPassword(email: string): any {
    return this.fireAuth.sendPasswordResetEmail(email);
  }

  logoutUser(): firebase.Promise<any> {
    this.membershipSubscription.unsubscribe();
    return this.afAuth.auth.signOut();
  }

  changePassword(password) {
    const user = firebase.auth().currentUser;
    return user.updatePassword(password);
  }

  async setGlobals(uid) {
    // this.db.object('/users/' + uid).first().subscribe(
    //   (snapshot) => {
    //     this.globals.userName = snapshot.user_name;
    //     console.log('this.globals.firstName', this.globals.firstName);
    //   }
    // );
    let snapshot = await this.db.object('/users/' + uid).take(1).toPromise();
    this.globals.userName = snapshot.user_name;
    // this.globals.membership = snapshot.membership;
    console.log('this.globals.userName', this.globals.userName);
    // console.log('this.globals.membership', this.globals.membership);
  }

  unsetGlobals() {
    this.globals.userName = "";
    this.globals.current_userUID = "";
    this.globals.videoURL = "";
    this.globals.email = "";
    this.globals.dob;
    this.globals.contact = "";
    this.globals.address = "";
    this.globals.secondaryContact = "";
    this.globals.videos = [];
    // this.globals.publishableKey = "pk_test_iog9Th3sTbuvIIqoNoAKtX1O";
    this.globals.membership = "";
  }

  getProfilePhoto() {
    return this.afAuth.auth.currentUser;
  }

  getPaymentStatus() {
    this.membershipSubscription = this.db.object('/users/' + this.globals.current_userUID + '/membership/')
      .subscribe(
        (response) => {
          this.globals.membership = response.$value;
        }
      )
    // return this.db.object('/users/' + this.globals.current_userUID + '/membership/');
  }

  async updatePhoto(url: string, userName) {
    return await this.afAuth.auth.currentUser.updateProfile({
      displayName: userName,
      photoURL: url
    });
  }
}