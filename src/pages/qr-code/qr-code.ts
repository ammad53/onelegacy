import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, Platform } from 'ionic-angular';
import { QRCodeComponent } from 'angular2-qrcode';
import { GlobalVariable } from '../../app/globals';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { SocialSharing } from "@ionic-native/social-sharing";

@IonicPage()
@Component({
  selector: 'page-qr-code',
  templateUrl: 'qr-code.html',
})
export class QrCode {

  qrcodeValue: string = "";

  constructor(public navCtrl: NavController,
    private menu: MenuController,
    public navParams: NavParams,
    public platform: Platform,
    private globals: GlobalVariable,
    private socialSharing: SocialSharing,
    private barcodeScanner: BarcodeScanner) {

    this.menu.swipeEnable(true);
    this.qrcodeValue = this.globals.current_userUID;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QrCode');
  }

  shareQrCode() {

    this.barcodeScanner.encode(this.barcodeScanner.Encode.TEXT_TYPE, this.qrcodeValue)
      .then(value => {
        console.log(value);
        if (this.platform.is('ios')) {
          let file = 'file://' + value.file;
          // this.socialSharing.share(this.globals.userName + "'s", "test", file)
          //   .then((data) => console.log(data))
          //   .catch((error) => console.error(error));
          let message = this.globals.userName + "'s videos"
          this.socialSharing.share(message, "videos", value.file)
            .then((data) => console.log(data))
            .catch((error) => console.error(error));
        }

      });
  }

}
