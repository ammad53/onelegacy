import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, MenuController } from 'ionic-angular';
import { FirebaseService } from '../../providers/firebase-service';

@IonicPage()
@Component({
  selector: 'page-member-ship',
  templateUrl: 'member-ship.html',
})
export class MemberShip {

  membership: string = "Basic";
  paymentStatus: string;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public menu: MenuController,
    private fs: FirebaseService) {

    this.menu.swipeEnable(true);
    this.getPaymentStatus();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MemberShip');
    // this.getPaymentStatus();
  }

  stripeModal(status: string, upgrade: boolean) {
    let payment = this.modalCtrl.create(
      "StripeModal",
      {
        status: status,
        upgrade: upgrade,
        currentStatus: this.paymentStatus
      });
    payment.present();
    payment.onDidDismiss(async (data) => {
      console.log(data);
      if (data) {
        await this.fs.updateMembershipStatus(data.status);
        this.getPaymentStatus();
      } else console.log("stripe modal dismissed");
    })
  }

  getPaymentStatus() {
    // const paymentStatus = await this.fs.getPaymentStatus();
    const ref = this.fs.getPaymentStatusList()
    ref.once("value", (data) => {
      console.log("data from ref", data.val());
      this.paymentStatus = data.val();
    })

  }
}
