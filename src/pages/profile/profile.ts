import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController, LoadingController } from 'ionic-angular';
import { FirebaseService } from "../../providers/firebase-service";
import { Camera, CameraOptions } from '@ionic-native/camera';
import { AuthService } from "../../providers/auth-service";


@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  userObject: any;
  profilePhoto: string;
  userName: string;

  cameraOptions: CameraOptions = {
    quality: 20,
    destinationType: this.camera.DestinationType.DATA_URL,
    sourceType: this.camera.PictureSourceType.CAMERA,
    encodingType: this.camera.EncodingType.JPEG,
    targetWidth: 250,
    targetHeight: 250,
    saveToPhotoAlbum: true
  };
  pickerOptions: CameraOptions = {
    quality: 20,
    destinationType: this.camera.DestinationType.DATA_URL,
    sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
    encodingType: this.camera.EncodingType.JPEG,
    targetWidth: 250,
    targetHeight: 250,
    saveToPhotoAlbum: true
  };

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private camera: Camera,
    private as: AuthService,
    private fs: FirebaseService) {

    this.userObject = this.fs.getUserData();
    this.profilePhoto = this.as.getProfilePhoto().photoURL;
    this.userName = this.as.getProfilePhoto().displayName;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

  changeImage() {
    let alert = this.alertCtrl.create({
      title: "Upload an image",
      buttons: [
        {
          text: 'Take an image',
          handler: () => {
            this.takePicture();
          }
        },
        {
          text: 'Pick an image',
          handler: () => {
            this.pickPickture();
          }
        }
      ]
    });
    alert.present();
  }

  changePassword() {
    let modal = this.modalCtrl.create("ChangePasswordPage");
    modal.present();
  }

  changeNumber(type) {
    let modal = this.modalCtrl.create("ChangeNumberPage", { contactType: type });
    modal.present();
  }

  async takePicture() {
    try {
      let loader = this.loadingCtrl.create({
        content: "Upladoing image"
      });
      const image = await this.camera.getPicture(this.cameraOptions);
      loader.present();
      const savedImage = await this.fs.storeProfilePhoto(image);
      await this.as.updatePhoto(savedImage, this.userName);
      this.profilePhoto = this.as.getProfilePhoto().photoURL;
      loader.dismiss();
    } catch (error) {
      console.error(error);
    }
  }
  async pickPickture() {
    try {
      let loader = this.loadingCtrl.create({
        content: "Upladoing image"
      });
      const image = await this.camera.getPicture(this.pickerOptions);
      loader.present();
      const savedImage = await this.fs.storeProfilePhoto(image);
      await this.as.updatePhoto(savedImage, this.userName);
      this.profilePhoto = this.as.getProfilePhoto().photoURL;
      loader.dismiss();
    } catch (error) {
      console.error(error);
    }
  }

}
