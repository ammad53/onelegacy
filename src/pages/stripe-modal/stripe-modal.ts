import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, LoadingController, App, MenuController, Events } from 'ionic-angular';
import { GlobalVariable } from "../../app/globals";
import { Stripe } from '@ionic-native/stripe';
import { NativeStorage } from '@ionic-native/native-storage';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

// import { FirebaseService } from "../../providers/firebase-service";
import { PaymentService } from '../../providers/payment-service';
import { FirebaseService } from '../../providers/firebase-service';

@IonicPage()
@Component({
  selector: 'page-stripe-modal',
  templateUrl: 'stripe-modal.html',
})
export class StripeModal {

  public submitAttempt: boolean = false;
  public validCard: boolean = false;
  public validCvv: boolean = false;
  public validExpDate: boolean = false;
  loader: any;
  public stripeForm: FormGroup;
  maxYear; minYear;
  status: string;
  upgrade: boolean;
  currentStatus: string;
  displayAmount: string;
  chargeAmount: string;
  chargType: string;

  constructor(
    // private app: App,
    private events: Events, 
    private menu: MenuController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public formBuilder: FormBuilder,
    private fs: FirebaseService,
    private ps: PaymentService,
    private stripe: Stripe,
    private nativeStorage: NativeStorage,
    private globals: GlobalVariable) {

    this.menu.swipeEnable(false);
    this.status = navParams.get("status");
    this.upgrade = navParams.get("upgrade");
    this.currentStatus = navParams.get("currentStatus");

    this.stripeForm = formBuilder.group({
      cardNumber: ['', Validators.compose([Validators.pattern('[0-9]{16}'), Validators.required])],
      cvv: ['', Validators.compose([Validators.pattern('[0-9]{3}'), Validators.required])],
      expMonth: ['', Validators.required],
      expYear: ['', Validators.required]
    })

    this.loader = loadingCtrl.create({
      content: "Please wait...",
      // dismissOnPageChange: true
    })

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StripeModal');
  }

  ionViewWillEnter() {
    let currentYear = new Date().getFullYear();
    this.maxYear = currentYear + 5;
    this.minYear = currentYear;
    console.log("maxYear: ", this.maxYear);
    console.log("minYear: ", this.minYear);
    this.stripe.setPublishableKey(this.globals.publishableKey);
    this.figureTheAmount();
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  async pay() {
    this.submitAttempt = true;
    if (!this.stripeForm.valid) {
      console.log(' Some values were not given or were incorrect, please fill them');
    } else {
      let loader = this.loadingCtrl.create({
        content: "Please wait..."
      });
      loader.present();
      let cardNumber = this.stripeForm.value.cardNumber;
      let expYear = this.stripeForm.value.expYear;
      let expMonth = this.stripeForm.value.expMonth;
      let cvv = this.stripeForm.value.cvv;
      let card = {
        number: cardNumber,
        expMonth: expMonth,
        expYear: expYear,
        cvc: cvv
      };

      console.log(this.stripeForm.value);
      try {
        const validation = await Promise.all(
          [
            this.stripe.validateCardNumber(cardNumber),
            this.stripe.validateCVC(cvv),
            this.stripe.validateExpiryDate(expMonth, expYear)
          ]
        ).catch(error => { throw (error) });
        console.log("card validations", validation);
        // const token = await this.createCardToken(cardNumber, expYear, expMonth, cvv);
        const token = await this.stripe.createCardToken(card)
        // this.charge(token);
        console.log('token: ', token);
        const stripeResponse = await this.ps.stripePayment(this.chargeAmount, token, "process fees");
        console.log(stripeResponse);
        await loader.dismiss();
        // this.trigerEvent(this.chargType);
        this.showSuccessAlert("Payment successfull", this.chargType);
      } catch (error) {
        
        loader.dismiss();
        this.showErrorAlert(error);
        console.error("card error: ", error);
      }
    }
  }

  figureTheAmount() {

    switch (this.status) {
      case "Basic": {
        console.log("this.status", this.status);
        this.displayAmount = "$450";
        this.chargeAmount = "45000";
        this.chargType = "Basic"
        break;
      }
      case "Standard": {
        console.log("this.status", this.status);

        if (this.upgrade == true) {
          this.displayAmount = "$350";
          this.chargeAmount = "35000";
          this.chargType = "Standard";
        } else {
          this.displayAmount = "$800";
          this.chargeAmount = "80000";
          this.chargType = "Standard";
        }
        break;
      }
      case "Premium": {
        console.log("this.status", this.status);

        if (this.upgrade == true) {
          console.log("this.upgrade", this.upgrade);
          console.log("this.currentStatus", this.currentStatus);
          if (this.currentStatus == 'Basic') {
            console.log('this.currentStatus', this.currentStatus);
            this.displayAmount = "$1050";
            this.chargeAmount = "105000";
            this.chargType = "Premium";

          } else if (this.currentStatus == 'Standard') {
            this.displayAmount = "$700";
            this.chargeAmount = "70000";
            this.chargType = "Premium";
          }
        } else {
          this.displayAmount = "$1500";
          this.chargeAmount = "150000";
          this.chargType = "Premium";
        }
        break;
      }
    }
  }

  showErrorAlert(error) {
    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: error,
      buttons: ['Retry']
    });
    alert.present();
  }

  showSuccessAlert(success: string, status?) {
    let alert = this.alertCtrl.create({
      title: 'Success!',
      subTitle: success,
      buttons: ['OK']
    });
    alert.present();
    alert.onDidDismiss(
      () => this.viewCtrl.dismiss({
        msg: "payment successfull",
        status: status
      })
    );
  }

  // trigerEvent(satus: string) {
  //   this.events.publish("paymentStatus", status);
  // }

}
