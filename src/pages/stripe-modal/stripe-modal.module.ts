import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StripeModal } from './stripe-modal';

@NgModule({
  declarations: [
    StripeModal,
  ],
  imports: [
    IonicPageModule.forChild(StripeModal),
  ],
  exports: [
    StripeModal
  ]
})
export class StripeModalModule {}
