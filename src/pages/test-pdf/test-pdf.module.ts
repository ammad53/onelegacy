import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TestPdfPage } from './test-pdf';
import { PdfViewerComponent } from "ng2-pdf-viewer";

@NgModule({
  declarations: [
    TestPdfPage,
    PdfViewerComponent
  ],
  imports: [
    IonicPageModule.forChild(TestPdfPage),
  ],
  exports: [
    TestPdfPage
  ]
})
export class TestPdfPageModule {}
