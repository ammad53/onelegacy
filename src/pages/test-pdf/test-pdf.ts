import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { PdfViewerComponent } from "ng2-pdf-viewer";
import { InAppBrowser, InAppBrowserOptions } from "@ionic-native/in-app-browser";

@IonicPage()
@Component({
  selector: 'page-test-pdf',
  templateUrl: 'test-pdf.html',
})
export class TestPdfPage {

  pdfSrc: string = "https://firebasestorage.googleapis.com/v0/b/onelegacy-f0695.appspot.com/o/termsfeedtermsconditionspdfenglish.pdf?alt=media&token=bead7447-231b-47f9-a7da-3aa7e3b13df8";
  page: number = 1;
  zoom: number = 0;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    private iab: InAppBrowser) {

    // this.platform.ready()
    //   .then(
    //   () => {
    //     let options: InAppBrowserOptions = {
    //       location: 'yes',
    //       clearcache: 'yes'
    //     };
    //     iab.create(this.pdfSrc, '_system', options).show();
    //   }
    //   ).catch(error => console.error(error));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TestPdfPage');
  }

  zoomIn() {
    this.zoom += 1;
    console.log(this.zoom);
  }

  zoomOut() {
    if (this.zoom <= 0) {
      this.zoom = 0;
    } else {
      this.zoom -= 1;
    }
    console.log(this.zoom);
  }

}
/* var options = {
		location: 'yes',
		clearcache: 'yes'
	};

	$cordovaInAppBrowser.open('http://dlucca-abrhsp.tempspace.no/uploads/materials/07/ff/07ff6180309811e68542739bcce0a3a9.pdf', '_system', options)
     .then(function (event) {
         alert("success");
     })
     .catch(function (event) {
         alert("error");
     }); */