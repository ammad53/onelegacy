import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FirebaseService } from "../../providers/firebase-service";


@IonicPage()
@Component({
  selector: 'page-change-number',
  templateUrl: 'change-number.html',
})
export class ChangeNumberPage {

  submitAttempt: boolean = false;
  numberForm: FormGroup;
  contactType: string;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    private formBuilder: FormBuilder,
    private fs: FirebaseService) {

    this.numberForm = formBuilder.group({
      phone: ['', Validators.compose([Validators.minLength(6), Validators.required])],
    });

    this.contactType = navParams.get('contactType');
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangeNumberPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  async changeNumber() {
    let loader = this.loadingCtrl.create({
      content: "Changing number...",
      dismissOnPageChange: true,
    });
    this.submitAttempt = true;
    if (!this.numberForm.valid) {
      console.log(' Some values were not given or were incorrect, please fill them');
    } else {
      loader.present();
      try {
        console.log(this.numberForm.value);
        await this.fs.changeNumber(this.contactType, this.numberForm.value.phone);
        await loader.dismiss();
        let alert = this.alertCtrl.create({
          title: "Number change successfully",
          buttons: ["OK"]
        });
        alert.present();
        alert.onWillDismiss(() => this.viewCtrl.dismiss());
      } catch (error) {
        loader.dismiss();
        console.error(error);
      }
    }
  }

}
