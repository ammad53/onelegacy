import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChangeNumberPage } from './change-number';

@NgModule({
  declarations: [
    ChangeNumberPage,
  ],
  imports: [
    IonicPageModule.forChild(ChangeNumberPage),
  ],
  exports: [
    ChangeNumberPage
  ]
})
export class ChangeNumberPageModule {}
