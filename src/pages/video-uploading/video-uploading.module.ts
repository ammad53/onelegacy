import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VideoUploading } from './video-uploading';

@NgModule({
  declarations: [
    VideoUploading,
  ],
  imports: [
    IonicPageModule.forChild(VideoUploading),
  ],
  exports: [
    VideoUploading
  ]
})
export class VideoUploadingModule {}
