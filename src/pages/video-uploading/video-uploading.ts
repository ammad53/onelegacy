import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, LoadingController, AlertController, MenuController } from 'ionic-angular';
import { File } from '@ionic-native/file';
import { FirebaseService } from '../../providers/firebase-service';
import { GlobalVariable } from '../../app/globals';
import firebase from 'firebase';


@IonicPage()
@Component({
  selector: 'page-video-uploading',
  templateUrl: 'video-uploading.html',
})
export class VideoUploading {

  title: string = '';
  description: string = '';
  fileName: string;
  filePath: string;
  loader: any;
  progress: any;
  blobFile: any;
  // videos: Array<{ video: string, title: string, description: string }> = [];

  constructor(public navCtrl: NavController,
    private menu: MenuController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    private plt: Platform,
    private file: File,
    private fs: FirebaseService,
    private globals: GlobalVariable) {

    this.menu.swipeEnable(false);
    this.blobFile = navParams.get('blobFile');
    let videoData = navParams.get("videoData");
    this.filePath = videoData/*[0].fullPath*/;
    // this.fileName = videoData[0].name;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VideoUploading');
  }

  saveVideo(videoFile) {
    let filePath;
    let path = videoFile.substring(0, videoFile.lastIndexOf('/') + 1);
    let tempPath = path.substring(path.indexOf('e/') + 1, path.lastIndexOf('/') + 1);

    let name = videoFile.substring(videoFile.lastIndexOf('/') + 1, videoFile.length);

    if (this.plt.is('ios')) {
      filePath = "file://" + tempPath
    } else {
      filePath = path;
    }
    console.log("videoFile", videoFile);
    console.log("path", path);
    console.log("filePath", filePath);
    console.log("name", name);

    // this.loader = this.loadingCtrl.create({
    //   content: "Uploading Video",
    // });
    // this.loader.present();

    this.file.readAsArrayBuffer(filePath, name)
      .then(
      (success) => {
        console.log("success ", success);

        let blob = new Blob([success], { type: "video/mp4" });
        console.log(blob);
        this.uploadToFirebase(blob);
      }, (error) => {
        console.error(error);
      }
      )
  }

  async uploadToFirebase(blobFile) {
    let videoRef = this.fs.getVideoReferece();



    let loader = this.loadingCtrl.create({
      content: `Uploading Video`,
    });
    loader.present();
    try {

      this.globals.videos = await videoRef;
      console.log(this.globals.videos);
      let alertSuccess = this.alertCtrl.create({
        title: "Upload finished",
        buttons: ["OK"]
      });
      let alertError = this.alertCtrl.create({
        title: "Video is too large to upload",
        buttons: ["OK"]
      });

      let storageRef = firebase.storage().ref();
      // this.globals.videos
      let videoNumber = this.globals.videos.length;
      console.log(videoNumber)
      let userRef = storageRef.child(this.globals.current_userUID).child('videos').child(videoNumber.toString())

      let uploadTask = userRef.put(blobFile)

      uploadTask.on('state_changed', (snapshot) => {
        var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        loader.setContent(`Uploading Video ${Math.floor(progress).toString()}%`);
        console.log('Upload is ' + progress + '% done');

        if (progress == 100) {
          loader.dismiss().then(() => alertSuccess.present());
        }
        // loader.onDidDismiss(() => alert.present());

        switch (snapshot.state) {
          case firebase.storage.TaskState.PAUSED: // or 'paused'
            console.log('Upload is paused');
            break;
          case firebase.storage.TaskState.RUNNING: // or 'running'
            console.log('Upload is running');
            break;
          case firebase.storage.TaskState.SUCCESS:
            return 'Upload Success 1';
        }
      }, (error) => {
        loader.dismiss().then(() => alertError.present())
        console.error("error uploading", error);
        return error;
      }, () => {
        var downloadURL = uploadTask.snapshot.downloadURL;
        console.log("downloadURL", downloadURL);
        this.globals.videoURL = downloadURL;

        // this.globals.videos.push({ video: downloadURL, title: this.title, description: this.description });
        let value = { video: downloadURL, title: this.title, description: this.description, storageNumber: videoNumber };
        // this.ns.setItem(this.globals.current_userUID, { videoURL: downloadURL });
        this.fs.addSingleVideo('videos', /*this.globals.videos*/value)
          .then((data) => {
            console.log("database data", data);
            this.navCtrl.popToRoot();
          }).catch(error => console.error(error));
      });
    } catch (error) {
      this.loader.dismiss();
      let alert = this.alertCtrl.create({
        title: "An error occured please try again",
        buttons: ["OK"]
      });
      alert.present();
    }
  }

}
