import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Events } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EmailValidator } from '../../validators/email';
import { PaymentService } from "../../providers/payment-service";


@IonicPage()
@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html',
})
export class SignUp {

  submitAttempt: boolean = false;
  signupForm: FormGroup;
  loader: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    private auth: AuthService,
    private ps: PaymentService,
    private events: Events) {

    this.signupForm = formBuilder.group({
      userName: ['',  Validators.compose([Validators.required, Validators.minLength(3), Validators.pattern('[a-zA-Z]*')])],
      email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
      dob: ['', Validators.required],
      contact: ['', Validators.compose([Validators.minLength(6), Validators.pattern('[0-9]*'), Validators.required])],
      address: ['', Validators.minLength(10)],
      secondaryContact: ['', Validators.compose([Validators.minLength(6), Validators.pattern('[[0-9]*')])],
      password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignUp');
  }

  async signup() {
    this.loader = this.loadingCtrl.create({
      content: "Signing Up...",
      dismissOnPageChange: true
    });
    this.submitAttempt = true;
    // let age = this.currentYear - this.signupForm.value.dob.substring(0, 4);
    // console.log(this.signupForm.value.dob.substring(0, 4));
    if (!this.signupForm.valid) {
      console.log(' Some values were not given or were incorrect, please fill them');
    } else {
      this.loader.present();
      console.log('success!');
      console.log(this.signupForm.value);
      const data = await this.auth.signupUser(
        this.signupForm.value.email,
        this.signupForm.value.password,
        this.signupForm.value);
      console.log(data);
      this.auth.getPaymentStatus();
      // this.events.publish("paymentStatus", paymentStatus);
      this.navCtrl.setRoot('Home');
    }
  }

  // async testGrid() {
  //   try {
  //     const sendGrid = await this.ps.sendGrid();
  //     console.log("sendGrid", sendGrid)
  //   } catch (error) {
  //     console.error(error)
  //   }

  // }

}
