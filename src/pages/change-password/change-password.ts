import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from "../../providers/auth-service";


@IonicPage()
@Component({
  selector: 'page-change-password',
  templateUrl: 'change-password.html',
})
export class ChangePasswordPage {

  submitAttempt: boolean = false;
  passwordForm: FormGroup;
  passMatch: boolean = true;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public viewCtrl: ViewController,
    private formBuilder: FormBuilder,
    private as: AuthService) {

    this.passwordForm = formBuilder.group({
      password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
      confirmPassword: ['', Validators.compose([Validators.minLength(6), Validators.required])],
    })

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangePasswordPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  async changePassword() {
    let loader = this.loadingCtrl.create({
      content: "Changing password...",
      dismissOnPageChange: true,
    });
    this.submitAttempt = true;

    if (!this.passwordForm.valid) {
      console.log(' Some values were not given or were incorrect, please fill them');
    } else if (this.passwordForm.value.password !== this.passwordForm.value.confirmPassword) {
      this.passMatch = false;
    } else {
      this.passMatch = true;
      loader.present();
      try {
        console.log(this.passwordForm.value);
        const passwordChange = await this.as.changePassword(this.passwordForm.value.password);
        console.log(passwordChange);
        await loader.dismiss()
        let alert = this.alertCtrl.create({
          title: "Password change successfully",
          buttons: ["OK"]
        });
        alert.present();
        alert.onWillDismiss(() => this.viewCtrl.dismiss());
      } catch (error) {
        await loader.dismiss();
        let alert = this.alertCtrl.create({
          title: error,
          buttons: ["Retry"]
        });
        alert.present();
        console.error(error);
      }
    }
  }
}
