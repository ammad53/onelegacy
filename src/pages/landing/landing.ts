import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, MenuController, Events, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GlobalVariable } from "../../app/globals";
import { AuthService } from '../../providers/auth-service';
import { FirebaseService } from '../../providers/firebase-service';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';
import { EmailValidator } from '../../validators/email';


@IonicPage()
@Component({
  selector: 'page-landing',
  templateUrl: 'landing.html',
})
export class Landing {

  public submitAttempt: boolean = false;
  public loginForm: FormGroup;
  loader: any;

  constructor(public navCtrl: NavController,
    public formBuilder: FormBuilder,
    private menu: MenuController,
    public toastCtrl: ToastController,
    private events: Events,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    private globals: GlobalVariable,
    private barcodeScanner: BarcodeScanner,
    private fs: FirebaseService,
    private auth: AuthService) {

    this.menu.swipeEnable(false);
    this.loginForm = formBuilder.group({
      email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
      password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Login');
  }

  async login() {
    this.loader = this.loadingCtrl.create({
      content: "Loggin In...",
      dismissOnPageChange: true
    });
    this.submitAttempt = true;
    if (!this.loginForm.valid) {
      console.log(' Some values were not given or were incorrect, please fill them');
    } else {
      this.loader.present();
      console.log('success!');
      console.log(this.loginForm.value);
      try {
        const data = await this.auth.loginUser(
          this.loginForm.value.email,
          this.loginForm.value.password, );
        this.globals.current_userUID = data.uid;
        this.auth.setGlobals(data.uid);
        this.auth.getPaymentStatus();
        // this.fs.getUserVideos();

        // const ref = this.fs.getPaymentStatusList()
        // ref.once("value", (data) => {
        //   console.log("data from ref", data.val());
        //   this.events.publish("paymentStatus", data.val());
        // })
        this.navCtrl.setRoot('Home');
      } catch (error) {
        this.loginAlert(error);
      }
    }
  }

  loginAlert(error) {
    this.loader.dismiss();
    let alert = this.alertCtrl.create({
      title: "Login error",
      subTitle: error,
      buttons: ["OK"]
    })
    alert.present();
  }
  goToSignup() {
    // this.navCtrl.setRoot('Register');
    this.navCtrl.push('SignUp')
  }

  // getPaymentStatus() {
  //   this.fs.getPaymentStatus().subscribe(
  //     (snapshot) => {
  //       // this.paymentStatus = snapshot.$value;
  //       this.globals.membership = snapshot.$value;
  //     }
  //   );
  // }
  scanBarcode() {
    let options: BarcodeScannerOptions = {
      showFlipCameraButton: true,
      showTorchButton: true,
      formats: 'QR_CODE'
    }

    this.barcodeScanner.scan(options).then((barcodeData) => {
      console.log("Success! Barcode data is here", barcodeData);
      let userSubscription = this.fs.getOtherUsersVideos(barcodeData.text)
        .take(1).subscribe(
        (snapshot) => {
          console.log(snapshot);
          if (snapshot.length > 0) {
            this.navCtrl.push("OtherUsersVideos", { videos: snapshot, uid: barcodeData.text });
          } else {
            let toast = this.toastCtrl.create({
              message: "Can't find any vidoes",
              duration: 3000,
              position: 'bottom'
            });

            toast.onDidDismiss(() => {
              console.log('Dismissed toast');
            });

            toast.present();
          }
          userSubscription.unsubscribe();

        })
    }, (err) => {
      console.error("error scanning", err);
    });
  }

}