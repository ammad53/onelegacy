import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, MenuController, LoadingController } from 'ionic-angular';
import { GlobalVariable } from '../../app/globals';
// import { NativeStorage } from '@ionic-native/native-storage';
import { FirebaseService } from '../../providers/firebase-service';
import firebase from 'firebase';
import { Subscription } from "rxjs";
import { StreamingMedia, StreamingVideoOptions } from "@ionic-native/streaming-media";


@IonicPage()
@Component({
  selector: 'page-my-videos',
  templateUrl: 'my-videos.html',
})
export class MyVideos {

  noVideosUploadedYet: boolean;
  videoSubscription: Subscription;
  videos: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public menu: MenuController,
    public alertCtrl: AlertController,
    private globals: GlobalVariable,
    private streamingMedia: StreamingMedia,
    private fs: FirebaseService,
    /*private ns: NativeStorage*/) {
    menu.swipeEnable(false);
    this.videos = this.fs.getUserVideosToDisplay();
    this.videoSubscription = this.fs.getUserVideosToDisplay()
      .subscribe((snapshot) => {
        console.log(snapshot);
      })
    // const videos = this.fs.getUserData()
    //   .subscribe(
    //   (snapshot) => {
    //     console.log(snapshot);
    //     videos.unsubscribe();
    //   }
    //   )
    // if (this.globals.videos.length == 0) {
    //   this.noVideosUploadedYet = true;
    // } else {
    //   this.noVideosUploadedYet = false;
    // }

    console.log("this.noVideosUploadedYet", this.noVideosUploadedYet);

  }
  ionViewWillLeave() {
    this.menu.swipeEnable(true);
    this.videoSubscription.unsubscribe();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyVideos');

  }
  play(url: string, title: string) {
    // let loader = this.loadingCtrl.create({
    //   content: `Playing ${title}`
    // })
    // loader.present();
    let options: StreamingVideoOptions = {
      successCallback: () => { console.log('Video played') },
      errorCallback: (e) => { console.log('Error streaming') },
      orientation: 'landscape'
    };
    this.streamingMedia.playVideo(url, options);
  }
  deleteVideo(/*index*/key, storageNumber) {
    let alert = this.alertCtrl.create({
      title: "Are you sure you want to delete this video",
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Okay',
          handler: data => {
            console.log('Ok clicked');
            this.deleteFromDb(key, storageNumber);
          }
        }
      ]
    });
    alert.present();
  }

  deleteFromDb(key, storageNumber) {
    console.log(key);
    console.log(storageNumber);
    this.fs.delteVideo(key)
      .then((data) => console.log(data))
      .then(() => this.fs.getUserVideos())
      .catch((error) => console.error("error deleting video", error));
    let storageRef = firebase.storage().ref();
    let videoRef = storageRef.child(this.globals.current_userUID).child('videos').child(storageNumber.toString())
    videoRef.delete()
      .then(() => console.log("deleted from storage"))
      .catch((error) => console.error("error deleting from storage", error))
  }
  // async getVideo() {
  //   try {
  //     this.noVideosUploadedYet = false;
  //     const data = await this.ns.getItem(this.globals.current_userUID);
  //     this.globals.videoURL = data.videoURL;
  //   } catch (error) {
  //     this.noVideosUploadedYet = true;
  //   }
  // }
}
