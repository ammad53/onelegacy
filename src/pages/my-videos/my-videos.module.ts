import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyVideos } from './my-videos';

@NgModule({
  declarations: [
    MyVideos,
  ],
  imports: [
    IonicPageModule.forChild(MyVideos),
  ],
  exports: [
    MyVideos
  ]
})
export class MyVideosModule {}
