import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FirebaseService } from '../../providers/firebase-service';


@IonicPage()
@Component({
  selector: 'page-other-users-videos',
  templateUrl: 'other-users-videos.html',
})
export class OtherUsersVideos {
  videos: any;
  name: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private fs: FirebaseService) {

    this.videos = navParams.get("videos");
    let uid = navParams.get("uid");
    this.fs.getUserName(uid)
      .once('value', (name) => {
        this.name = name.val();
        console.log(name.val());
      })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OtherUsersVideos');
  }

}
