import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ToastController, MenuController, LoadingController } from 'ionic-angular';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';
import { MediaCapture, CaptureVideoOptions, MediaFile, CaptureError } from '@ionic-native/media-capture';
import { File } from '@ionic-native/file';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FirebaseService } from '../../providers/firebase-service';
import { GlobalVariable } from '../../app/globals';
// import { VideoEditor } from '@ionic-native/video-editor';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class Home {

  videoOptions: CaptureVideoOptions;
  moreOptions: MediaFile;
  fileName: string;
  filePath: string;
  localUrl: string;
  userData: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    private menu: MenuController,
    private plt: Platform,
    private barcodeScanner: BarcodeScanner,
    private mediaCapture: MediaCapture,
    private file: File,
    private camera: Camera,
    private fs: FirebaseService,
    private globals: GlobalVariable,
    private toastCtrl: ToastController) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Home');
  }
  ionViewWillEnter() {
    this.menu.swipeEnable(true);
    const userSubscription = this.fs.getUserVideosToDisplay()
      .take(1)
      .subscribe(
      (snapshot) => {
        console.log(snapshot);
        this.userData = snapshot;
        // userSubscription.unsubscribe();
      }
      );
  }

  async takeVideo(method) {
    console.log("method", method);
    console.log("this.globals.membership", this.globals.membership)
    switch (this.globals.membership) {
      case "Free": {
        let toast = this.toastCtrl.create({
          message: 'You must a member to upload videos',
          duration: 3000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        toast.present();
        break;
      }
      case "Basic": {
        if (this.userData && this.userData.length >= 1) {
          // if (this.userData.length >= 1) {
          let toast = this.toastCtrl.create({
            message: "Upgrade to standard or premium to upload more videos",
            duration: 3000,
            position: 'bottom'
          });

          toast.onDidDismiss(() => {
            console.log('Dismissed toast');
          });

          toast.present();
          // }
        } else {
          if (method == 'fromCamera') {
            this.recordVideo();
          }
          else if (method == "fromFile") this.selectVideo();
        }
        break;
      }
      case "Standard": {
        if (this.userData && this.userData.length >= 3) {
          let toast = this.toastCtrl.create({
            message: "Upgrade to premium to upload more videos",
            duration: 3000,
            position: 'bottom'
          });

          toast.onDidDismiss(() => {
            console.log('Dismissed toast');
          });

          toast.present();
        } else {
          if (method == 'fromCamera') {
            this.recordVideo();
          }
          else if (method == "fromFile") this.selectVideo();
        }
        break;
      }
      case "Premium": {
        if (this.userData && this.userData.length >= 10) {
          let toast = this.toastCtrl.create({
            message: "You can't upload more than 10 videos",
            duration: 3000,
            position: 'bottom'
          });

          toast.onDidDismiss(() => {
            console.log('Dismissed toast');
          });

          toast.present();
        } else {
          if (method == 'fromCamera') {
            this.recordVideo();
          }
          else if (method == "fromFile") this.selectVideo();
        }
        break;
      }
    }

  }

  recordVideo() {
    try {

      this.videoOptions = {
        // limit: 1,
        duration: 10
      };
      let options: CaptureVideoOptions = { duration: 180, quality: 0 };

      this.mediaCapture.captureVideo(options)
        .then(
        (data: MediaFile[]) => {
          console.log(data)
          this.filePath = data[0].fullPath;
          this.fileName = data[0].name;
          this.localUrl = data[0]['localURL'];

          data[0].getFormatData(data => {
            console.log("data.duration", data.duration);
          }, error => console.error(error));
          // this.navCtrl.push("VideoUploading", { videoData: this.filePath });
          this.prepareVideo(this.filePath);
        },
        (error: CaptureError) => console.error("video capturing error", error)
        )
      // .then(() => this.uploadToFirebase(this.filePath));

    } catch (error) {
      console.error("video capturing error", error);
    }
  }

  async prepareVideo(videoFile) {
    let filePath;
    let path = videoFile.substring(0, videoFile.lastIndexOf('/') + 1);
    let tempPath = path.substring(path.indexOf('e/') + 1, path.lastIndexOf('/') + 1);

    let name = videoFile.substring(videoFile.lastIndexOf('/') + 1, videoFile.length);

    if (this.plt.is('ios')) {
      filePath = "file://" + tempPath
    } else {
      filePath = path;
    }
    console.log("videoFile", videoFile);
    console.log("path", path);
    console.log("filePath", filePath);
    console.log("name", name);

    let loader = this.loadingCtrl.create({
      content: "Preparing Video",
    });
    loader.present();

    this.file.readAsArrayBuffer(filePath, name)
      .then(
      (success) => {
        console.log("success ", success);

        let blob = new Blob([success], { type: "video/mp4" });
        console.log(blob);
        loader.dismiss();
        this.navCtrl.push("VideoUploading", { blobFile: blob });
      }, (error) => {
        console.error(error);
        loader.dismiss()
      }
      )
  }

  scanBarcode() {
    let options: BarcodeScannerOptions = {
      showFlipCameraButton: true,
      showTorchButton: true,
      formats: 'QR_CODE'
    }

    this.barcodeScanner.scan(options).then((barcodeData) => {
      console.log("Success! Barcode data is here", barcodeData);
      let userSubscription = this.fs.getOtherUsersVideos(barcodeData.text)
        .take(1).subscribe(
        (snapshot) => {
          console.log(snapshot);
          if (snapshot.length > 0) {
            this.navCtrl.push("OtherUsersVideos", { videos: snapshot, uid: barcodeData.text });
          } else {
            let toast = this.toastCtrl.create({
              message: "Can't find any vidoes",
              duration: 3000,
              position: 'bottom'
            });

            toast.onDidDismiss(() => {
              console.log('Dismissed toast');
            });

            toast.present();
          }
          userSubscription.unsubscribe();
        })
    }, (err) => {
      console.error("error scanning", err);
    });
  }

  myVideos() {
    if (this.userData.length > 0) {
      this.navCtrl.push("MyVideos");
    } else {
      let toast = this.toastCtrl.create({
        message: "You have'nt upload any videos",
        duration: 3000,
        position: 'bottom'
      });

      toast.onDidDismiss(() => {
        console.log('Dismissed toast');
      });

      toast.present();
    }
  }

  async selectVideo() {
    let options: CameraOptions = {
      sourceType: 0,
      mediaType: 1

    };
    try {
      let data = await this.camera.getPicture(options);
      let videoFile = "file://" + data;
      console.log("video data:", videoFile);
      // this.uploadToFirebase(videoFile);
      // this.navCtrl.push("VideoUploading", { videoData: videoFile });
      this.prepareVideo(videoFile)
    } catch (error) {
      console.error(error);
    }
  }

}
