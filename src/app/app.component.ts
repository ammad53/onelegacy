import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, MenuController, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Keyboard } from "@ionic-native/keyboard";
// import { /*AuthProviders,*/ /*AuthMethods */} from 'angularfire2';
import { AngularFireAuth } from 'angularfire2/auth';
import { AuthService } from '../providers/auth-service';
import { GlobalVariable } from './globals';
import { FirebaseService } from '../providers/firebase-service';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any/* = 'Landing'*/;
  paymentStatus: string/* = "Testing"*/;
  subscription: any;

  constructor(platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    private menu: MenuController,
    private as: AuthService,
    private fs: FirebaseService,
    private globals: GlobalVariable,
    private afAuth: AngularFireAuth,
    private events: Events,
    private keyboard: Keyboard) {

    const authObserver = afAuth.authState.subscribe(user => {
      this.menu.swipeEnable(false);
      if (user) {
        // this.rootPage = TabsPage;
        console.log(user);
        console.log("logging from app component: ", user.uid);
        globals.current_userUID = user.uid;
        this.as.setGlobals(user.uid);
        this.as.getPaymentStatus();
        this.rootPage = 'Home';
        authObserver.unsubscribe();
        // this.checkAccountStatus(user.uid);
        // this.backgroundCheckStatus(user.uid);
      } else {
        this.rootPage = 'Landing';
        authObserver.unsubscribe();
      }
    });
    platform.ready().then(() => {
      this.keyboard.disableScroll(true);
      this.keyboard.hideKeyboardAccessoryBar(false);
      // this.events
      //   .subscribe('paymentStatus', data => {
      //     this.paymentStatus = data;
      //     this.globals.membership = data;
      //     if (this.paymentStatus !== "Free") {
      //       this.getPaymentStatus();
      //       this.events.unsubscribe("paymentStatus");
      //     }
      //   });
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  qrCode() {
    this.menu.close();
    this.nav.setRoot("QrCode");
  }

  home() {
    this.menu.close();
    this.nav.setRoot("Home");
  }

  membership() {
    this.menu.close();
    this.nav.setRoot("MemberShip");
  }

  aboutUs() {
    this.menu.close();
    this.nav.setRoot("AboutUsPage");
  }

  contactUs() {
    this.menu.close();
    this.nav.setRoot("ContactUsPage");
  }

  profile() {
    this.menu.close();
    this.nav.setRoot("ProfilePage");
  }

  async logout() {
    await this.as.logoutUser();
    this.as.unsetGlobals();
    this.menu.close();
    this.nav.setRoot("Landing")
  }

  testPdf() {
    this.nav.setRoot("TestPdfPage");
    this.menu.close();
  }

  // getPaymentStatus() {
  //   /*this.subscription = */this.fs.getPaymentStatus().subscribe(
  //     (snapshot) => {
  //       this.paymentStatus = snapshot.$value;
  //       this.globals.membership = snapshot.$value;
  //     }
  //   );
  // }
}
